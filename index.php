<?php
interface File {
    public function read($path);
    public function write($path, $some);
}


interface Client {
    public function buy ($id);
    public function repayment($id);
}

class Shop implements File , Client{
    public function read($path) {
        echo "Считываем из файла и возвращаем строку<br />";
    }
    public function write($path, $some) {
        echo "Записываем в файл данные $some<br />";
    }
    public function buy($id) {
        echo "Спасибо за покупку<br />";
        $this->write("data.db", "Был куплен товар $id");
    }
    public function repayment($id) {
        $this->read("data.db");
        $this->write("data.db", "Был сделан возврат товара $id");
    }
}

$shop = new Shop();
$shop->buy(5);
$shop->repayment(5);



echo "poliformizm"."<br>";


class kalk{
    public function area($data){
        echo "square";
        $s=$data*$data;
        echo $s."<br>";
    }
    public function exception($data)
    {
        echo "exception"."<br>";
    }
}


class konst extends kalk {
 function exception($data)
 {
     echo 'circle';
     $S=Pi()*2;
     echo $S."<br>";
 }
}
$a= new kalk();
$b=new konst();

$a->area(4);
$a->exception(3);
$b->exception(7);

